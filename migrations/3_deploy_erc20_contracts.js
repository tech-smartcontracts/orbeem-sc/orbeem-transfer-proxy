const ERC20TransferProxy = artifacts.require('ERC20TransferProxy');

module.exports = async function (deployer) {
	await deployer.deploy(ERC20TransferProxy, { gas: 1500000 });
	const eRC20TransferProxy = await ERC20TransferProxy.deployed();
	await eRC20TransferProxy.__ERC20TransferProxy_init({ gas: 200000 });
};